#!/bin/bash

n=$(echo $1 | awk 'BEGIN { FS="." } {print $1}')
magick convert $1 $n.png
rm $1

echo "#################################"
echo "Convertito $1 in $n.png"
echo "#################################"
